import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorEditionComponent } from './sensor-edition.component';

describe('SensorEditionComponent', () => {
  let component: SensorEditionComponent;
  let fixture: ComponentFixture<SensorEditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorEditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorEditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
