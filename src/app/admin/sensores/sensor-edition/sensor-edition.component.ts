import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Sensormodel} from '../../../models/sensor.model';
import { SensorService } from "../../../services/sensor.service";
import { Globals } from '../../../globals';
import { ToastrService } from 'ngx-toastr';
import {Observable} from 'rxjs/Rx';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-sensor-edition',
  templateUrl: './sensor-edition.component.html',
  styleUrls: ['./sensor-edition.component.css'],
  providers: [Globals, SensorService]
})
export class SensorEditionComponent implements OnInit {

  id = null;
  user;
  operacao: string = "Inserir";
  acao: string = "Salvar";
  SensorModel = new Sensormodel();
  company = {"companyId":null};

  constructor(private route: ActivatedRoute,
    private _toastr:ToastrService,
    private _sensor:SensorService, 
    private _globals: Globals) {
    this.route.params.subscribe( param => this.id = param.id);
   
    this._globals.getUserdata().catch((res: any) => Observable.throw(res))
        .subscribe(
          (res)  => {
            this.user = res;
            this.company.companyId = res.company.id;
          },
          error => {
          }
        );
  }

  ngOnInit() {
    if(this.id){
      this.getSensorData(this.id)
      this.operacao = "Editar";
      this.acao = "Atualizar";
    }
  }

  getSensorData(id) {
    $('#cover-spin').show(10);
    this._sensor.getSensorByID(this.id).subscribe(
      (res)  => {
        this.SensorModel = res;
        $('#cover-spin').hide(10);
      },
      error => {
        this._toastr.error('Sensor não encontrado', 'Erro!');
        $('#cover-spin').hide(10);
      }
    );
  }
  
  processSensor() {
    if(this.id){
      this.updateSensor();
    }else{
      this.insertSensor();
    }
  }

  updateSensor() {
    $('#cover-spin').show(10);
    if(this.SensorModel.code == ""){
      this._toastr.error('Atenção!', 'Preencha os campos obrigatórios.');
    }else if(this.SensorModel.barcode == ""){
      this._toastr.error('Atenção!', 'Preencha os campos obrigatórios.');
    }else{
      this.SensorModel.company_id = this.user.company.id;
      this._sensor.updateSensors(this.id, this.SensorModel).catch((res: any) => Observable.throw(res))
      .subscribe(
        (res)  => {
          if(res.code=="1" || res.code=="6"){
            this._toastr.success('Registro salvo corretamente.', 'Sucesso!');
          }else{
            this._toastr.error('Reinicie a página e tente novamente.', 'Erro!');
          }
          $('#cover-spin').hide(10);
        },
        error => {
          this._toastr.error('Reinicie a página e tente novamente.', 'Erro!');
          $('#cover-spin').hide(10);
        }
      );
    }
  }

  insertSensor() {
    $('#cover-spin').show(10);
    if(this.SensorModel.code == ""){
      this._toastr.error('Atenção!', 'Preencha os campos obrigatórios.');
    }else if(this.SensorModel.barcode == ""){
      this._toastr.error('Atenção!', 'Preencha os campos obrigatórios.');
    }else{
      this.SensorModel.company_id = this.user.company.id;
      this._sensor.saveSensor(this.SensorModel).catch((res: any) => Observable.throw(res))
      .subscribe(
        (res)  => {
          if(res.code=="1" || res.code=="6"){
            this._toastr.success('Registro salvo corretamente.', 'Sucesso!');
            this.SensorModel = new Sensormodel();
          }else{
            this._toastr.error('Reinicie a página e tente novamente.', 'Erro!');
          }
          $('#cover-spin').hide(10);
        },
        error => {
          this._toastr.error('Reinicie a página e tente novamente.', 'Erro!');
          $('#cover-spin').hide(10);
        }
      );
    }
  }

}
