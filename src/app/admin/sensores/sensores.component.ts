import { Component, OnInit } from '@angular/core';
import { Globals } from '../../globals';
import { SensorService } from '../../services/sensor.service';
import {Observable} from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-sensores',
  templateUrl: './sensores.component.html',
  styleUrls: ['./sensores.component.css'],
  providers: [Globals, SensorService, NgbModal, NgbActiveModal]
})
export class SensoresComponent implements OnInit {

  constructor(private _sensor:SensorService, 
    private _toastr:ToastrService,
    private _globals: Globals,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal) { 
      this.page = 0;
    }

    SensorList;
    company = {"companyId":null};
    code = {"code":null};
    user;
    page = 0;
    exclusionMark;
    modalref;

    open(content, id) {
      this.exclusionMark = id;
      this.modalref = this.modalService.open(content, {size: 'lg'});
    }

    excluir(){
      $('#cover-spin').show(10);
      this._sensor.deleteSensor(this.exclusionMark).subscribe(
        (res)  => {
          this.exclusionMark = null;
          this.modalref.close();
          this._toastr.success('Registro excluído corretamente.', 'Sucesso!');
          this._sensor.getSensorByCompanyAndPage(this.company, this.page).subscribe(
            (res)  => {
              this.SensorList = res;
              $('#cover-spin').hide(10);
            },
            error => { 
              this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
              $('#cover-spin').hide(10);
            }
          );
        },
        error => { 
          this._toastr.error('Erro ao excluir funcionário.', 'Erro!'); 
          $('#cover-spin').hide(10);
        }
      );
    }
  
    ngOnInit() {
      $('#cover-spin').show(10);
      this._globals.getUserdata().catch((res: any) => Observable.throw(res))
      .subscribe(
        (res)  => {
          this.user = res;
          this.company.companyId = res.company.id;        
          this._sensor.getSensorByCompanyAndPage(this.company, this.page).subscribe(
            (res)  => {
              this.SensorList = res;
              $('#cover-spin').hide(10);
            },
            error => { 
              this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
              $('#cover-spin').hide(10);
            }
          );
        },
        error => { 
          this._toastr.error('Erro ao acessar servidor.', 'Erro!');
          $('#cover-spin').hide(10);
        }
      );
    }

    searchSensors(){
      $('#cover-spin').show(10);
      this._sensor.getSensorByCode(this.code).subscribe(
        (res)  => {
          this.SensorList = res;
          $('#cover-spin').hide(10);
        },
        error => { 
          this._toastr.error('Erro ao recuperar lista de sensores.', 'Erro!'); 
          $('#cover-spin').hide(10);
        }
      );
    }

    getPage(page){
      $('#cover-spin').show(10);
      this._sensor.getSensorByCompanyAndPage(this.company, page).subscribe(
        (res)  => {
          this.SensorList = res;
          $('#cover-spin').hide(10);
        },
        error => { 
          this._toastr.error('Erro ao recuperar lista de sensores.', 'Erro!'); 
          $('#cover-spin').hide(10);
        }
      );
    }



}
