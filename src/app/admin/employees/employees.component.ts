import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { Globals } from '../../globals';
import { EmployeeService } from '../../services/employee.service';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SensorAssocModel } from '../../models/sensorAssoc.model';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeeService, Globals, NgbModal, NgbActiveModal]
})
export class EmployeesComponent implements OnInit {

  constructor(private _employee: EmployeeService,
    private _globals: Globals,
    private _toastr: ToastrService,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal) {
      this.page = 0;
    }

  employeeList;
  company = {'companyId': null};
  name = {'name': null};
  user;
  modalref;
  sensorModalRef;
  exclusionMark;
  page;
  sensorAttachObj = new SensorAssocModel();
  code = '';
  barcode = '';

  open(content, id) {
    this.exclusionMark = id;
    this.modalref = this.modalService.open(content, {size: 'lg'});
  }

  attach(sensorModal, id) {
    this.exclusionMark = id;
    this.sensorModalRef = this.modalService.open(sensorModal, {size: 'lg'});
  }

  adicionarSensor () {
    $('#cover-spin').show(10);
    this.sensorAttachObj.code = this.code;
    this.sensorAttachObj.barcode = this.barcode;
    this.sensorAttachObj.employee_id = this.exclusionMark;
    this._employee.attachSensor(this.sensorAttachObj).catch((res: any) => Observable.throw(res)).subscribe(
      (res)  => {
        if(res.code == 4) {
          this._toastr.error('Sensor não encontrado! Verifique o código.', 'Erro!');
        } else {
          this._toastr.success('Sensor corretamente atribuído ao Funcionário.', 'Sucesso!');
          this.sensorModalRef.close();
        }
        $('#cover-spin').hide(10);
      },
      error => { 
        this._toastr.error('Erro ao realizar associação.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );
  }

  excluir() {
    $('#cover-spin').show(10);
    this._employee.deleteEmployee(this.exclusionMark).subscribe(
      (res)  => {
        this.exclusionMark = null;
        this.modalref.close();
        this._toastr.success('Registro excluído corretamente.', 'Sucesso!');
        this._employee.getEmployeeByCompanyAndPage(this.company, this.page).subscribe(
          (res)  => {
            this.employeeList = res;
            $('#cover-spin').hide(10);
          },
          error => { 
            this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
            $('#cover-spin').hide(10);
          }
        );
      },
      error => { 
        this._toastr.error('Erro ao excluir funcionário.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );
  }

  ngOnInit() {
    $('#cover-spin').show(10);
    this._globals.getUserdata().catch((res: any) => Observable.throw(res))
    .subscribe(
      (res)  => {
        this.user = res;
        this.company.companyId = res.company.id;
        this._employee.getEmployeeByCompany(this.company).subscribe(
          (res)  => {
            this.employeeList = res;
            $('#cover-spin').hide(10);
          },
          error => { 
            this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
            $('#cover-spin').hide(10);
          }
        );
      },
      error => {
        this._toastr.error('Erro ao acessar servidor.', 'Erro!');
        $('#cover-spin').hide(10);
      }
    );
  }

  searchEmployee() {
    $('#cover-spin').show(10);
    this._employee.getEmployeeByName(this.name).subscribe(
      (res)  => {
        this.employeeList = res;
        $('#cover-spin').hide(10);
      },
      error => { 
        this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );
  }

  getPage(page) {
    this.page = page;
    $('#cover-spin').show(10);
    this._employee.getEmployeeByCompanyAndPage(this.company, page).subscribe(
      (res)  => {
        this.employeeList = res;
        $('#cover-spin').hide(10);
      },
      error => { 
        this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );
  }

}
