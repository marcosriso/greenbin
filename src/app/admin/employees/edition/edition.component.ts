import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Observable} from 'rxjs/Rx';
import { HttpResponse } from '@angular/common/http';
import { Globals } from '../../../globals';

import { EmployeeModel } from "../../../models/employee.model";
import { EmployeeService } from "../../../services/employee.service";
import { ToastrService } from 'ngx-toastr';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-edition',
  templateUrl: './edition.component.html',
  styleUrls: ['./edition.component.css'],
  providers: [EmployeeService, Globals]
})
export class EditionComponent implements OnInit {

  id = null;
  operacao: string = "Inserir";
  acao: string = "Salvar";
  public user;

  EmployeeModel = new EmployeeModel();
  

  constructor(private route: ActivatedRoute, 
    private _employee: EmployeeService, 
    private _toastr:ToastrService,
    private _globals: Globals) 
  {
    this.route.params.subscribe( param => this.id = param.id);
    
    this._globals.getUserdata().catch((res: any) => Observable.throw(res))
        .subscribe(
          (res: HttpResponse<any>)  => {
            this.user = res;
          },
          error => {
          }
        );
      
    
  }

  ngOnInit() {
    if(this.id){
      this.getEmployeeData(this.id);
      this.operacao = "Editar";
      this.acao = "Atualizar";
    }
  }

  getEmployeeData(id) {
    $('#cover-spin').show(10);
    this._employee.getEmployeeByID(this.id).subscribe(
      (res)  => {
        this.EmployeeModel = res;
        $('#cover-spin').hide(10);
      },
      error => {
        this._toastr.error('Funcionário não encontrado', 'Erro!');
        $('#cover-spin').hide(10);
      }
    );
  }

  processEmployee() {
    if(this.id){
      this.updateEmployee();
    }else{
      this.insertEmployee();
    }
  }

  updateEmployee() {
    $('#cover-spin').show(10);
    if(this.EmployeeModel.name == ""){
      this._toastr.error('Atenção!', 'Preencha os campos obrigatórios.');
    }else{
      this.EmployeeModel.company_id = this.user.company.id;
      this._employee.updateEmployees(this.id, this.EmployeeModel).catch((res: any) => Observable.throw(res))
      .subscribe(
        (res)  => {
          if(res.code=="1" || res.code=="6"){
            this._toastr.success('Registro atualizado corretamente.', 'Sucesso!');
          }else{
            this._toastr.error('Reinicie a página e tente novamente.', 'Erro!');
          }
          $('#cover-spin').hide(10);
        },
        error => {
          this._toastr.error('Erro de servidor.', 'Erro!');
          $('#cover-spin').hide(10);
        }
      );
    }
  }

  insertEmployee() {
    $('#cover-spin').show(10);
    if(this.EmployeeModel.name == ''){
      this._toastr.error('Atenção!', 'Preencha os campos obrigatórios.');
    }else{
      this.EmployeeModel.company_id = this.user.company.id;
      this._employee.saveEmployees(this.EmployeeModel).catch((res: any) => Observable.throw(res))
      .subscribe(
        (res)  => {
          if(res.code=="1"){
            this._toastr.success('Registro salvo corretamente.', 'Sucesso!');
            this.EmployeeModel = new EmployeeModel();
          }else{
            this._toastr.error('Erro de servidor.', 'Erro!');
          }
          $('#cover-spin').hide(10);
        },
        error => {
          this._toastr.error('Erro de servidor.', 'Erro!');
          $('#cover-spin').hide(10);
        }
      );
    }
  }

}
