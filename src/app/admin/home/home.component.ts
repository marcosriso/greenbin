import { Component, OnInit } from '@angular/core';
import {CookService} from '../../services/mycookie.service';
import {UserService} from '../../services/user.service';
import { Globals } from '../../globals';
import {Observable} from 'rxjs/Rx';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [CookService, UserService, Globals]
})
export class HomeComponent implements OnInit {

  public user;

  constructor(private _cookie: CookService, private _user: UserService, private _globals: Globals) { 
    this._globals.getUserdata().catch((res: any) => Observable.throw(res))
        .subscribe(
          (res: HttpResponse<any>)  => {
            this.user = res;
          },
          error => {
          }
        );
    
  }

  ngOnInit() {
    this._cookie.checkCredentials();
  }

  logOut(){
    this._cookie.clearToken();
  }


}
