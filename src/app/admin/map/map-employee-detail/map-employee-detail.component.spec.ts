import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapEmployeeDetailComponent } from './map-employee-detail.component';

describe('MapEmployeeDetailComponent', () => {
  let component: MapEmployeeDetailComponent;
  let fixture: ComponentFixture<MapEmployeeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapEmployeeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapEmployeeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
