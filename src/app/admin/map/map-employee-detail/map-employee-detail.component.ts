import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import {CookService} from '../../../services/mycookie.service';
import { EmployeeService } from '../../../services/employee.service';
import { Globals } from '../../../globals';
import { ToastrService } from 'ngx-toastr';
import {Observable} from 'rxjs/Rx';
import {ActivatedRoute} from "@angular/router";
import {SensorService} from '../../../services/sensor.service';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-map-employee-detail',
  templateUrl: './map-employee-detail.component.html',
  styleUrls: ['./map-employee-detail.component.css'],
  providers: [EmployeeService, SensorService, Globals]
})
export class MapEmployeeDetailComponent implements OnInit {

  constructor(private _cookie: CookService, 
    private _employee: EmployeeService,
    private _globals: Globals,
    private _sensor: SensorService,
    private _toastr: ToastrService,
    private route: ActivatedRoute) { }

  today = new Date();
  dd = this.today.getDate();
  mm = this.today.getMonth()+1; //January is 0!
  yyyy = this.today.getFullYear();
  date: any = { date: { year: this.yyyy, month: this.mm, day: this.dd } };
  getObj;
  smm = "";
  sdd = "";
      
  user;
  id = null;
  company = {'companyId': null};
  employeeData;
  url;
  sensorHistory;
  markers = [];
  h = 0;
  userDistance = 0;
  kmUserDistance = "";
  lines = [];

  public myDatePickerOptions: IMyDpOptions = {
    dayLabels:	{su: 'Dom', mo: 'Seg', tu: 'Ter', we: 'Qua', th: 'Qui', fr: 'Sex', sa: 'Sab'},
    monthLabels:{ 1: 'Jan', 2: 'Fev', 3: 'Mar', 4: 'Abr', 5: 'Mai', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Set', 10: 'Out', 11: 'Nov', 12: 'Dez' },
    todayBtnTxt: "Hoje",
    editableDateField: false,
    dateFormat: 'dd/mm/yyyy',
  };

  @ViewChild('gmapdetail') gmapElement: any;
  map: google.maps.Map;
  mapProp = {};


  ngOnInit() {
    this._cookie.checkCredentials();

    this.route.params.subscribe( param => this.id = param.id);
    this.getData();

  }

  ngAfterContentInit(){
    this.h = $(window).height() - 150;
  }


  getData(){
    $('#cover-spin').show(10);
    this._employee.getEmployeeByIDandSensor(this.id).subscribe(
      (res)  => {
        
        this.employeeData = res;
        let lat= this.employeeData.sensor.lastKnowPosition.latitude;
        let lng= this.employeeData.sensor.lastKnowPosition.longitude;

        
        this.url = 'https://maps.googleapis.com/maps/api/staticmap?center='+lat+','+lng+'&zoom=15&size=260x240&maptype=roadmap'+
          '&markers=color:blue%7Clabel:+%7C'+lat+','+lng +
          '&key=AIzaSyAyIttyfsNzWZ_jKxzYXD4ssvb0-7E1sN4';
        
        this.mapProp = {
          center: new google.maps.LatLng(lat, lng),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.gmapElement.nativeElement, this.mapProp);

        this.setDateAndMarks();
        
      },
      error => { 
        this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );
  }

  setDateAndMarks(){
    this.clear();
    if(this.date.date.month < 10) {
      this.smm = '0'+this.date.date.month.toString();
    }else{
      this.smm = this.date.date.month.toString();
    }

    if(this.date.date.day < 10) {
      this.sdd = '0'+this.date.date.day.toString();
    }else{
      this.sdd = this.date.date.day.toString();
    }
    this.getObj = { employeeId:<number> this.id*1, startdate: this.date.date.year + '-' + this.smm + '-' + this.sdd, enddate: this.date.date.year + '-' + this.smm + '-' + this.sdd };

    this._sensor.sensorDataByEmployeeAndDate(this.getObj).catch((res: any) => Observable.throw(res)).subscribe(
      (res)  => {

        var bounds = new google.maps.LatLngBounds();
        var i = 0;

        this.markers = [];
        this.userDistance = 0;
        var icon = {};
        
        var infowindow = new google.maps.InfoWindow();

        for (let e of res) {

          var contentString = '<b>' + e.employee.name + '</b><br>';

          var d = new Date(e.terminal.date);
          var datestring = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" +
          d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);

          if(e.terminal.date) {
            contentString += 'Data e Hora: ' + datestring + '<br>';
          }

          contentString += 'Posição: ' + e.terminal.latitude + ', ' + e.terminal.longitude;
          


          if(i==0) {
            icon = {
              url: '/assets/start.png',
              size: new google.maps.Size(22, 22),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(6, 0)
            };

          }else if(i == res.length - 1){
            icon = '/assets/end.png';
          }else{
            icon = '/assets/1pxnull.png'
          }

          let marker = new google.maps.Marker({
            position: new google.maps.LatLng(e.terminal.latitude, e.terminal.longitude),
            map: this.map,
            icon: icon,
            title: contentString
          })


          google.maps.event.addListener(marker, 'click', function(){
            infowindow.close();
            infowindow.open(this.map, this);
            infowindow.setContent(this.title);
          });

          this.markers.push(marker);
          bounds.extend(marker.getPosition());
          i++;
        }

        if(i > 1) {
          this.map.fitBounds(bounds);
        }

        $('#cover-spin').hide(10);
        this.computeDistance();
        
      },
      error => { 
        this._toastr.error('Erro ao recuperar histórico do sensor.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );
  }

  clear() {
    for(let e of this.markers) {
      e.setMap(null);
    }

    for(let e of this.lines) {
      e.setMap(null);
    }
  }

  onDateChanged(event: IMyDateModel) {
    this.date.date = event.date;
    this.setDateAndMarks();
  }

  addMarkers(){
    var marker1 = new google.maps.Marker({
      position: new google.maps.LatLng(this.employeeData.sensor.lastKnowPosition.latitude, this.employeeData.sensor.lastKnowPosition.longitude),
      map: this.map,
      icon: '/assets/icon.png',
      title: this.employeeData.name
    });

  }

  rad(x) {
    return x * Math.PI / 180;
  };
  
  computeDistance(){
    var i = 0;
    var z = 1;
    this.userDistance = 0;
    for(i=0; i < this.markers.length; i++) {
      if(typeof this.markers[z] !== 'undefined'){
        var dist = google.maps.geometry.spherical.computeDistanceBetween(this.markers[i].getPosition(), this.markers[z].getPosition());
        this.userDistance = this.userDistance + dist;

        var line = new google.maps.Polyline({
          path: [
            this.markers[i].getPosition(), 
            this.markers[z].getPosition()
          ],
          strokeColor: "#FF0000",
          strokeOpacity: 1.0,
          strokeWeight: 3,
          geodesic: true,
          map: this.map
        });

        this.lines.push(line);

      }


      z++;
    }
    this.kmUserDistance = (Math.round(this.userDistance) / 1000).toFixed(2).replace(".", ",");

    
  }



  getDistance(p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = this.rad(p2.lat() - p1.lat());
    var dLong = this.rad(p2.lng() - p1.lng());
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat())) * Math.cos(this.rad(p2.lat())) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
  };

}
