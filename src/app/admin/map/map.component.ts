import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import {CookService} from '../../services/mycookie.service';
import { EmployeeService } from '../../services/employee.service';
import { Globals } from '../../globals';
import { ToastrService } from 'ngx-toastr';
import {Observable} from 'rxjs/Rx';
import {ActivatedRoute} from "@angular/router";
import {SensorService} from '../../services/sensor.service';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [EmployeeService, SensorService, Globals]
})
export class MapComponent implements OnInit {

  markers = [];
  user;
  company = {'companyId': null};
  mapProp = {};
  h = 0;

  constructor(private _cookie: CookService, 
    private _employee: EmployeeService,
    private _globals: Globals,
    private _sensor: SensorService,
    private _toastr: ToastrService,
    private route: ActivatedRoute) {
      this._cookie.checkCredentials();
     }

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;



  ngOnInit() {
    this._cookie.checkCredentials();
    $('#cover-spin').show(10);
    this._globals.getUserdata().catch((res: any) => Observable.throw(res))
      .subscribe(
        (res)  => {
          this.user = res;
          this.company.companyId = res.company.id;  
          this.addMarkers();
        },
        error => {
          this._toastr.error('Erro ao acessar servidor.', 'Erro!');
          $('#cover-spin').hide(10);
        }
      );

  }

  
  ngAfterContentInit(){
    this.h = $(window).height() - 150;
  }

  addMarkers(){
    this._sensor.employeesByCompanyWithSensorNoPage(this.company).catch((res: any) => Observable.throw(res)).subscribe(
      (res)  => {
        var bounds = new google.maps.LatLngBounds();
        var i = 0;
        var lat = this.user.company.lat;
        var lng = this.user.company.lng;
        var zoom = 4;
        var contentString = '';

        if(res.length > 0) {
          if(res[0].sensor != null && res[0].sensor.lastKnowPosition) {
            lat = res[0].sensor.lastKnowPosition.latitude;
            lng = res[0].sensor.lastKnowPosition.longitude;
          }else if(res[1].sensor != null && res[1].sensor.lastKnowPosition) {
            lat = res[1].sensor.lastKnowPosition.latitude;
            lng = res[1].sensor.lastKnowPosition.longitude;
          }else if(res[0]){
            lat = res[0].company.lat;
            lng = res[0].company.lng;
          }
        }

        this.mapProp = {
          center: new google.maps.LatLng(lat, lng),
          zoom: zoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.gmapElement.nativeElement, this.mapProp);
        var infowindow = new google.maps.InfoWindow();
    
        if(res.length > 0) {
          for (let e of res) {
            
            contentString = '';

            if(e.sensor == null){
              continue;
            }

            if(e.sensor.lastKnowPosition == null){
              continue;
            }

            contentString = '<b>' + e.name + '</b><br>';

            if(e.sensor.lastKnowPosition.date) {
              var d = new Date(e.sensor.lastKnowPosition.date);
              var datestring = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" +
              d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
              contentString += 'Data e Hora: ' + datestring + '<br>';
            }

            contentString += 'Posição: ' + e.sensor.lastKnowPosition.latitude + ', ' + e.sensor.lastKnowPosition.longitude;

            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(e.sensor.lastKnowPosition.latitude, e.sensor.lastKnowPosition.longitude),
              map: this.map,
              icon: '/assets/mks.png',
              title: contentString
            })

            google.maps.event.addListener(marker, 'click', function(){
              infowindow.close();
              infowindow.open(this.map, this);
              infowindow.setContent(this.title);
            });


            this.markers.push(marker);
            bounds.extend(marker.getPosition());
            i++;
          }

          if(i > 1) {
            this.map.fitBounds(bounds);
          }
        }

        
        $('#cover-spin').hide(10);
        
      },
      error => { 
        this._toastr.error('Erro ao recuperar dados.', 'Erro!'); 
        $('#cover-spin').hide(10);
      }
    );

  }

}
