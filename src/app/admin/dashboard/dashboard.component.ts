import { Component, OnInit } from '@angular/core';
import {CookService} from '../../services/mycookie.service';
import { EmployeeService } from '../../services/employee.service';
import { Globals } from '../../globals';
import { ToastrService } from 'ngx-toastr';
import {Observable} from 'rxjs/Rx';
import { SensorAssocModel } from '../../models/sensorAssoc.model';
import { IMyDpOptions } from 'mydatepicker';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [EmployeeService, Globals]
})
export class DashboardComponent implements OnInit {

  company = {'companyId': null};
  user;
  page = 0;
  employeeList;
  sensorAttachObj = new SensorAssocModel();
  

  constructor(private _cookie: CookService, 
    private _employee: EmployeeService,
    private _globals: Globals,
    private _toastr: ToastrService) { 
      this.page = 0;
    }

    
  public myDatePickerOptions: IMyDpOptions = {
    dayLabels:	{su: 'Dom', mo: 'Seg', tu: 'Ter', we: 'Qua', th: 'Qui', fr: 'Sex', sa: 'Sab'},
    monthLabels:{ 1: 'Jan', 2: 'Fev', 3: 'Mar', 4: 'Abr', 5: 'Mai', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Set', 10: 'Out', 11: 'Nov', 12: 'Dez' },
    todayBtnTxt: "Hoje",
    editableDateField: false,
    dateFormat: 'dd/mm/yyyy',
  };

  ngOnInit() {
    this._cookie.checkCredentials();
    $('#cover-spin').show(10);
    this._globals.getUserdata().catch((res: any) => Observable.throw(res))
    .subscribe(
      (res)  => {
        this.user = res;
        this.company.companyId = res.company.id;
        this._employee.getEmployeeByCompanyIdWithSensor(this.company, this.page).subscribe(
          (res)  => {
            this.employeeList = res;
            $('#cover-spin').hide(10);
          },
          error => { 
            this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); 
            $('#cover-spin').hide(10);
          }
        );
      },
      error => {
        this._toastr.error('Erro ao acessar servidor.', 'Erro!');
        $('#cover-spin').hide(10);
      }
    );
  }

  dettach (sensorcode) {
    $('#cover-spin').show(10);
    this.sensorAttachObj.code = sensorcode;
    this.sensorAttachObj.barcode = null;
    this.sensorAttachObj.employee_id = null;

    this._employee.sensorDetach(this.sensorAttachObj).catch((res: any) => Observable.throw(res)).subscribe(
      (res)  => {
        $('#cover-spin').hide(10);
        if(res.code == 4) {
          this._toastr.error('Sensor não encontrado! Verifique o código.', 'Erro!');
        } else {
          this._toastr.success('Sensor desacoplado ao Funcionário.', 'Sucesso!');

          this._employee.getEmployeeByCompanyIdWithSensor(this.company, this.page).subscribe(
            (res)  => {
              this.employeeList = res;
            },
            error => { this._toastr.error('Erro ao recuperar lista de funcionários.', 'Erro!'); }
          );

        }
      },
      error => { 
        this._toastr.error('Erro ao realizar associação.', 'Erro!');
        $('#cover-spin').hide(10);
      }
    );
  }




}
