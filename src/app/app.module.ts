import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouterLinkActive } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './services/token.interceptor';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { MainComponent } from './home/main/main.component';
import { HomeComponent } from './admin/home/home.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { ProfileComponent } from './admin/profile/profile.component';
import { MapComponent } from './admin/map/map.component';

import { CookieService } from 'ngx-cookie-service';
import { CookService } from './services/mycookie.service';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { EmployeesComponent } from './admin/employees/employees.component';
import { EditionComponent } from './admin/employees/edition/edition.component';
import { SensoresComponent } from './admin/sensores/sensores.component';
import { SensorEditionComponent } from './admin/sensores/sensor-edition/sensor-edition.component';
import { MapEmployeeDetailComponent } from './admin/map/map-employee-detail/map-employee-detail.component';
import { MyDatePickerModule } from 'mydatepicker';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserlistComponent } from './admin/userlist/userlist.component';

const appRoutes: Routes = [
  { path: '', component: MainComponent },
  { path: 'admin', component: HomeComponent, 
    children: [
      {path: '', redirectTo: 'painel', pathMatch: 'full'},
      {path: 'painel', component: DashboardComponent},
      {path: 'perfil', component: ProfileComponent},
      {path: 'map', component: MapComponent},
      {path: 'map/funcionario/:id', component: MapEmployeeDetailComponent},
      {path: 'funcionarios', component: EmployeesComponent},
      {path: 'funcionarios/editar/:id', component: EditionComponent},
      {path: 'funcionarios/inserir', component: EditionComponent},
      {path: 'sensores', component: SensoresComponent},
      {path: 'sensores/editar/:id', component: SensorEditionComponent},
      {path: 'sensores/inserir', component: SensorEditionComponent},
      {path: 'usuarios', component: UserlistComponent}
    ] 
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    MapComponent,
    EmployeesComponent,
    EditionComponent,
    SensoresComponent,
    SensorEditionComponent,
    MapEmployeeDetailComponent,
    UserlistComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    ),
    FormsModule, HttpModule, HttpClientModule,CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), NgbModule.forRoot(), MyDatePickerModule
  ],
  providers: [CookieService, CookService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }