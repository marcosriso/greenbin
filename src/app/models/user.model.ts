import { Companymodel } from './company.model';

export class Usermodel {
    id: number;
    name: string;
    email: string;
    active: number;
    username: string;
    company_id: number;
    birthday: string;
    company: any;

    constructor() {
        this.id = null;
        this.name = "";
        this.email = "";
        this.active = 1;
        this.username = "";
        this.birthday = "";
        this.birthday = "";
        this.company = new Companymodel();
      }
}