export class Companymodel {
    id: number;
    name: string;
    email: string;
    active: number;
    phone: string;
    cnpj: string;
    lat: string;
    lng: string;

    constructor() {
        this.name = "";
        this.email = "";
        this.active = 1;
        this.phone = "";
        this.cnpj = "";
        this.lat = "";
        this.lng = "";
      }
}