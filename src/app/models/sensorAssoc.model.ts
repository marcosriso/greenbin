export class SensorAssocModel {
    code: string;
    barcode: string;
    employee_id: number;

    constructor() {
        this.code = '';
        this.barcode = '';
        this.employee_id = null;
      }
}
