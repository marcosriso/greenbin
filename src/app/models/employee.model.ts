import { Deserializable } from "./deserializable.models"

export class EmployeeModel implements Deserializable<EmployeeModel>{

    id: number;
    name: string;
    email: string;
    department: string;
    company_id: number;
    active: number;
    position: string;
    phone: string;

    constructor(){
      this.id = null;
      this.name = "";
      this.email = ""; 
      this.department = ""; 
      this.company_id = null; 
      this.active = 1; 
      this.position = ""; 
      this.phone = ""; 
    }

    deserialize(input: any): EmployeeModel {
      Object.assign(this, input);
      return this;
    }

  }

