export class Sensormodel {
    id: number;
    code: string;
    barcode: string;
    active: number;
    company_id: number;
    employee_id: number;
    description: string;

    constructor() {
        this.id = null;
        this.code = "";
        this.barcode = "";
        this.active = 1;
        this.company_id = null;
        this.employee_id = null;
        this.description = "";
      }
}