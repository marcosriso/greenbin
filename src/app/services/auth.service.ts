import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';


@Injectable()
export class AuthService {

      //url = 'http://localhost:8080/login';
      url = 'http://iottechs.com.br:8080/login';

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    observe: 'response' as 'response'
  };

  attemptAuth(username: string, password: string): Observable<any> {
    const credentials = {username: username, password: password};
    return this.http.post(this.url, credentials, this.httpOptions);
  }

}