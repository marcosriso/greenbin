import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { EmployeeModel } from '../models/employee.model';


@Injectable()
export class EmployeeService {

    constructor(private http: HttpClient) {
    }

          //url = 'http://localhost:8080';
          url = 'http://iottechs.com.br:8080';

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'}),
        observe: 'response' as 'response'
    };

    getEmployees(): Observable<any> {
        return this.http.get(this.url+'/employees');
    }

    getEmployeeByID(id): Observable<any> {
        return this.http.get(this.url+'/employee/'+id);
    }

    saveEmployees(obj) {
        return this.http.post<EmployeeModel>(this.url+'/employees', obj);
    }

    updateEmployees(id, obj) {
        return this.http.post<EmployeeModel>(this.url+'/employees/'+id, obj);
    }

    getEmployeeByCompany(company) {
        return this.http.post(this.url + '/employeesByCompany', company);
    }

    getEmployeeByCompanyAndPage(company, page) {
        return this.http.post(this.url + '/employeesByCompany?page=' + page, company);
    }

    getEmployeeByName(name) {
        return this.http.post(this.url + '/employeeByName', name);
    }

    getEmployeeByCompanyIdWithSensor(company, page) {
        return this.http.post(this.url + '/employeesByCompanyWithSensor?page=' + page, company);
    }

    deleteEmployee(id) {
        return this.http.get(this.url + '/employee/delete/' + id);
    }

    attachSensor(assocObj) {
        return this.http.post(this.url + '/sensorAssociation', assocObj);
    }

    sensorDetach(assocObj) {
        return this.http.post(this.url + '/sensorDetach', assocObj);
    }

    getEmployeeByIDandSensor(id): Observable<any> {
        return this.http.get(this.url+'/employeesensor/'+id);
    }
    

    
}
