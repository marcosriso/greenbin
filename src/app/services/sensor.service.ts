import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Sensormodel } from '../models/sensor.model';

@Injectable()
export class SensorService {

    constructor(private http: HttpClient) {
    }

          //url = 'http://localhost:8080';
          url = 'http://iottechs.com.br:8080';

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'}),
        observe: 'response' as 'response'
    };

    getSensors(): Observable<any> {
        return this.http.get(this.url+'/sensorsByPage');
    }

    getSensorByID(id): Observable<any> {
        return this.http.get(this.url+'/sensor/'+id);
    }

    saveSensor(obj) {
        return this.http.post<Sensormodel>(this.url+'/sensors', obj);
    }
    
    updateSensors(id, obj) {
        return this.http.post<Sensormodel>(this.url+'/sensors/'+id, obj);
    }

    getSensorByCompany(company){
        return this.http.post(this.url+'/sensorsByCompany', company);
    }

    getSensorByCode(company){
        return this.http.post(this.url+'/sensorByCode', company);
    }

    getSensorByCompanyAndPage(company, page) {
        return this.http.post(this.url+'/sensorsByCompany?page='+page, company);
    }

    deleteSensor(id){
        return this.http.get(this.url+'/sensor/delete/'+id);
    }

    sensorDataByEmployeeAndDate(obj){
        return this.http.post(this.url+'/sensorHistoryByEmployeeAndDate', obj);
    }

    employeesByCompanyWithSensorNoPage(company){
        return this.http.post(this.url+'/employeesByCompanyWithSensorNoPage', company);
    }
}