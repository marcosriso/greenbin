import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';


@Injectable()
export class UserService {

    //url = 'http://localhost:8080/users/me';
    url = 'http://iottechs.com.br:8080/users/me';

    constructor(private http: HttpClient) {
    }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json'}),
        observe: 'response' as 'response'
    };

    getUser(): Observable<any> {
        return this.http.get(this.url);
    }

}