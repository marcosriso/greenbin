import {Injectable} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';


@Injectable()
export class CookService {

  constructor(private _router: Router, public _cookie: CookieService) {
  }


  saveToken(token){
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    this._cookie.set("access_token", token, expireDate);
    this._router.navigate(['/admin']);
  }
  
  checkCredentials(){
    if (!this._cookie.check('access_token')){
        this._router.navigate(['/']);
    }
  }

  getToken(){
    return this._cookie.get("access_token");
  }

  clearToken(){
    if (this._cookie.check('access_token')){
      this._cookie.delete("access_token");
      this._router.navigate(['/']);
    }
  }

}