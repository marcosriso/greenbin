import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Observable} from 'rxjs/Rx';
import {CookService} from '../../services/mycookie.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers: [AuthService, CookService]
})
export class MainComponent implements OnInit {
 
  constructor( private authService: AuthService, private _cookie: CookService) { }

  ngOnInit() {
  }

  username: string;
  password: string;
  message: string = null;

  login(): void {
    $('#cover-spin').show(10);
    this.authService.attemptAuth(this.username, this.password)
    .catch((res: any) => Observable.throw(res))
    .subscribe(
      (res: HttpResponse<any>)  => {
        if(res.status==200){
          this._cookie.saveToken(res.headers.get('Authorization'));
          this.message = null;
        }else{
          this.message = "Erro de autenticação."
        }
        $('#cover-spin').hide(10);
      },
      error => {
        if(error.status==403){
          this.message = "Erro de autenticação."
        }else if(error.status == 0) {
          this.message = "Servidor indisponível."
        }
        $('#cover-spin').hide(10);
      }
    );
  }


}
