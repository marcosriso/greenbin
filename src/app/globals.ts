import { Injectable } from "@angular/core";
import {UserService} from './services/user.service';
import {Observable} from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import {Usermodel} from './models/user.model';

@Injectable()
export class Globals {

    public user: any = new Usermodel();

    constructor(private _user: UserService) { 
        
    }

    getUserdata() {
      return this._user.getUser();
    }

}